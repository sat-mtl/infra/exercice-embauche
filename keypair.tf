resource "tls_private_key" "main" {
  algorithm = "RSA"
  rsa_bits  = 2048
}

resource "aws_key_pair" "main" {
  key_name   = local.uid
  public_key = tls_private_key.main.public_key_openssh
}
