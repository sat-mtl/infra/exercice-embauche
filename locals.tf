resource "random_pet" "main" {}

resource "random_password" "ubuntu_password" {
  length           = 16
  special          = true
  override_special = "_%@"
}

locals {
  uid             = random_pet.main.id
  ubuntu_password = random_password.ubuntu_password.result
}
