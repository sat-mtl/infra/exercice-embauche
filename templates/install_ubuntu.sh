#!/bin/bash

apt-get update
apt-get upgrade -y
apt-get install -y mysql-server wordpress

echo -e "${ubuntu_password}\n${ubuntu_password}" | passwd ubuntu
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config
systemctl restart sshd

curl https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar -o /usr/local/bin/wp
chmod +x /usr/local/bin/wp

bash /usr/share/doc/wordpress/examples/setup-mysql localhost
mv /etc/wordpress/config-localhost.php /etc/wordpress/config-default.php

cat > /etc/apache2/sites-available/000-default.conf <<EOF
<VirtualHost *:80>
  ServerName localhost
  DocumentRoot /usr/share/wordpress/
  DirectoryIndex index.php index.html
  ErrorLog /var/log/apache2/wp-error.log
  TransferLog /var/log/apache2/wp-access.log

  # wp-content in /var/lib/wordpress/wp-content
  Alias /wp-content /var/lib/wordpress/wp-content

  <Directory /usr/share/wordpress>
    Options FollowSymLinks
    <IfVersion < 2.3>
      Order allow,deny
      Allow from all
    </IfVersion>
    <IfVersion >= 2.3>
      Require all granted
    </IfVersion>
  </Directory>
  <Directory /var/lib/wordpress/wp-content>
    Options FollowSymLinks
    <IfVersion < 2.3>
      Order allow,deny
      Allow from all
    </IfVersion>
    <IfVersion >= 2.3>
      Require all granted
    </IfVersion>
  </Directory>
</VirtualHost>
EOF

a2enmod rewrite
a2enmod vhost_alias

systemctl restart apache2

curl http://localhost/wp-admin/install.php?step=2 \
  --data-urlencode "weblog_title=WordPress" \
  --data-urlencode "user_name=admin" \
  --data-urlencode "admin_password=n0n3OFurBiz" \
  --data-urlencode "admin_password2=n0n3OFurBiz" \
  --data-urlencode "admin_email=mrichard@sat.qc.ca" \
  --data-urlencode "Submit=Install+WordPress" \
  ;

systemctl stop mysql
chmod 700 /usr/share/wordpress
