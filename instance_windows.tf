data "aws_ami" "windows" {
  # ami-0f9ca19613ffbfd2b
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["Windows_Server-2022-English-Full-Base-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_network_interface" "windows" {
  subnet_id       = aws_subnet.main.id
  security_groups = [aws_default_security_group.main.id]
}

resource "aws_eip" "windows" {
  network_interface = aws_network_interface.windows.id
  vpc               = true
}

resource "aws_instance" "windows" {
  ami               = data.aws_ami.windows.id
  instance_type     = "t2.small"
  key_name          = aws_key_pair.main.id
  get_password_data = true
  user_data_base64  = base64encode(templatefile("${path.module}/templates/install_windows.xml", { ubuntu_password = local.ubuntu_password }))

  network_interface {
    network_interface_id = aws_network_interface.windows.id
    device_index         = 0
  }

  root_block_device {
    volume_size = 40
  }
}

output "windows_ip" {
  value = aws_eip.windows.public_ip
}

output "windows_login" {
  value = "Administrator"
}

output "windows_password" {
  value     = rsadecrypt(aws_instance.windows.password_data, tls_private_key.main.private_key_pem)
  sensitive = true
}
