resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "main" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "ca-central-1a"
}

resource "aws_default_security_group" "main" {
  vpc_id = aws_vpc.main.id

  ingress = [
    {
      description      = ""
      prefix_list_ids  = []
      security_groups  = []
      self             = true
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      protocol         = "icmp"
      from_port        = 0
      to_port          = 8
    },
    {
      description      = ""
      prefix_list_ids  = []
      security_groups  = []
      self             = true
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      protocol         = "tcp"
      from_port        = 22
      to_port          = 22
    },
    {
      description      = ""
      prefix_list_ids  = []
      security_groups  = []
      self             = true
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      protocol         = "tcp"
      from_port        = 80
      to_port          = 80
    },
    {
      description      = ""
      prefix_list_ids  = []
      security_groups  = []
      self             = true
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      protocol         = "tcp"
      from_port        = 443
      to_port          = 443
    },
    {
      description      = ""
      prefix_list_ids  = []
      security_groups  = []
      self             = true
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      protocol         = "tcp"
      from_port        = 3389
      to_port          = 3389
    }
  ]

  egress = [
    {
      description      = ""
      prefix_list_ids  = []
      security_groups  = []
      self             = true
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      protocol         = "-1"
      from_port        = 0
      to_port          = 0
    }
  ]
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
}

resource "aws_default_route_table" "main" {
  default_route_table_id = aws_vpc.main.default_route_table_id

  route = [
    {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.main.id

      destination_prefix_list_id = ""
      egress_only_gateway_id     = ""
      instance_id                = ""
      ipv6_cidr_block            = ""
      nat_gateway_id             = ""
      network_interface_id       = ""
      transit_gateway_id         = ""
      vpc_endpoint_id            = ""
      vpc_peering_connection_id  = ""
    }
  ]
}
