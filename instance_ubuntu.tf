data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = ["099720109477"] # Canonical

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_network_interface" "ubuntu" {
  subnet_id       = aws_subnet.main.id
  security_groups = [aws_default_security_group.main.id]
}

resource "aws_eip" "ubuntu" {
  network_interface = aws_network_interface.ubuntu.id
  vpc               = true
}

resource "aws_instance" "ubuntu" {
  ami              = data.aws_ami.ubuntu.id
  instance_type    = "t2.small"
  key_name         = aws_key_pair.main.id
  user_data_base64 = base64gzip(templatefile("${path.module}/templates/install_ubuntu.sh", { ubuntu_password = local.ubuntu_password }))

  network_interface {
    network_interface_id = aws_network_interface.ubuntu.id
    device_index         = 0
  }

  root_block_device {
    volume_size = 10
  }
}

output "ubuntu_ip" {
  value = aws_eip.ubuntu.public_ip
}

output "ubuntu_login" {
  value = "ubuntu"
}

output "ubuntu_password" {
  value     = local.ubuntu_password
  sensitive = true
}


output "ubuntu_ssh_key" {
  value     = tls_private_key.main.private_key_pem
  sensitive = true
}
